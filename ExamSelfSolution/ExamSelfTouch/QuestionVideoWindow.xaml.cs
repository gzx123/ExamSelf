﻿using System.Runtime.InteropServices;
using System.Windows;
using ExamSelfTouch.Tools;

namespace ExamSelfTouch
{
    /// <summary>
    /// Description for QuestionVideoOptionWindow.
    /// </summary>
    public sealed partial class QuestionVideoWindow : Window
    {
        private static QuestionVideoWindow _instance = new QuestionVideoWindow();

        /// <summary>
        /// Initializes a new instance of the QuestionVideoOptionWindow class.
        /// </summary>
        private QuestionVideoWindow()
        {
            InitializeComponent();
            FormTools.SetFormFullScreen(this);
            content.FontSize = name.FontSize + 10;
        }

        static QuestionVideoWindow()
        {
        }

        public static QuestionVideoWindow GetInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new QuestionVideoWindow();
                    return _instance;
                }
                else return _instance;
            }
        }

        public static void ClearInstance()
        {
            _instance.Close();
            _instance = null;
        }

        private void MediaPlayer_OnMediaEnded(object sender, RoutedEventArgs e)
        {
            mediaPlayer.Stop();
            mediaPlayer.Play();
        }

        private void MediaPlayer_OnLoaded(object sender, RoutedEventArgs e)
        {
            //mediaPlayer.Play();
        }

        bool isPlaying = true;
        private void mediaPlayer_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            if (isPlaying)
            {
                mediaPlayer.Pause();
                isPlaying = false;
            }
            else
            {
                isPlaying = true;
                mediaPlayer.Play();
            }

        }
    }
}