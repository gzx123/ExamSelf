﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using ExamSelfTouch.Message;
using GalaSoft.MvvmLight.Messaging;

namespace ExamSelfTouch.Tools
{
    public class FormTools
    {
        public static void SetFormFullScreen(Window window)
        {
            //设置窗体全屏
            window.Topmost = true;
            window.WindowState = System.Windows.WindowState.Maximized;
            window.Hide(); //先调用其隐藏方法 然后再显示出来,这样就会全屏,且任务栏不会出现.如果不加这句 可能会出现假全屏即任务栏还在下面.
            window.Show();
        }

        private FormTools() { }

        static FormTools() { }

        public static void LoadingWindow(bool? isShow)
        {
            if (!isShow.HasValue) return;
            if (isShow.Value)
                ExamSelfTouch.LoadingWindow.GetInstance.Show();
            else
                ExamSelfTouch.LoadingWindow.GetInstance.Hide();
        }

        public static void QuestionTextWindow(bool? isShow)
        {
            if (!isShow.HasValue) return;
            if (isShow.Value){
                ExamSelfTouch.QuestionTextWindow.GetInstance.Show();
                ExamSelfTouch.QuestionTextWindow.GetInstance.listbox.Focus();
            }
                
            else
                ExamSelfTouch.QuestionTextWindow.GetInstance.Hide();
        }

        public static void QuestionImageWindow(bool? isShow)
        {
            if (!isShow.HasValue) return;
            if (isShow.Value){
                ExamSelfTouch.QuestionImageWindow.GetInstance.Show();
                ExamSelfTouch.QuestionImageWindow.GetInstance.listbox.Focus();
            }
            else
                ExamSelfTouch.QuestionImageWindow.GetInstance.Hide();
        }

        public static void QuestionVideoWindow(bool? isShow)
        {
            if (!isShow.HasValue) return;
            if (isShow.Value){
                ExamSelfTouch.QuestionVideoWindow.GetInstance.Show();
                ExamSelfTouch.QuestionVideoWindow.GetInstance.listbox.Focus();
                ExamSelfTouch.QuestionVideoWindow.GetInstance.mediaPlayer.Play();
            }

            else
            {
                ExamSelfTouch.QuestionVideoWindow.GetInstance.mediaPlayer.Stop();
                ExamSelfTouch.QuestionVideoWindow.GetInstance.Hide();
            }
                
        }

        public static void MainWindow(bool? isShow)
        {
            if (!isShow.HasValue) return;
            if (isShow.Value)
                ExamSelfTouch.MainWindow.GetInstance.Show();
            else
                ExamSelfTouch.MainWindow.GetInstance.Hide();
        }

        /// <summary>
        /// 清空所有单例窗体
        /// </summary>
        internal static void ClearAllSingleWindow()
        {
            ExamSelfTouch.QuestionImageWindow.ClearInstance();
            ExamSelfTouch.QuestionTextWindow.ClearInstance();
            ExamSelfTouch.QuestionVideoWindow.ClearInstance();
        }

        /// <summary>
        /// 显示信息
        /// </summary>
        /// <param name="msg"></param>
        internal static void ShowMsgWindow(string msg)
        {
            new ShowMsgWindow().Show();
            Messenger.Default.Send<SendMsg>(new SendMsg(msg));
        }
    }
}
