﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamSelfTouch.Tools
{
    using System.Configuration;
    using System.Net;
    using System.Net.Sockets;

    using ExamSelfTouch.Model;

    using Newtonsoft.Json;

    class LogTool
    {
        static ILog log = LogManager.GetLogger("MyLogo4Net");
        private static IPEndPoint _ipEndPoint;
        static LogTool()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = config.AppSettings.Settings;
                var item = settings["LOGADDRESS"];
                if (item != null)
                {
                    string ip = item.Value;
                    IPAddress address = IPAddress.Parse(ip);
                    _ipEndPoint = new IPEndPoint(address, 34566);
                }
            }
            catch (Exception ex)
            {
                IPAddress address = IPAddress.Parse("58.213.157.99");
                _ipEndPoint = new IPEndPoint(address, 34566);
                //LogTool.WriteErrorLog(ex);
            }
             
        }

        public static void UploadLog(string content, EventType type)
        {
            try
            {
                UdpClient udp = new UdpClient();
                ClientTracer ct = new ClientTracer()
                {
                    clientId = 0,
                    clientName = Global.ClientName,
                    collection = "exam.selfk3",
                    content = content,
                    eventType = type,
                    serial = Global.LogSerial++
                };
                string c = JsonConvert.SerializeObject(ct);
                byte[] data = Encoding.UTF8.GetBytes(c);
                udp.Send(data, data.Length, _ipEndPoint);
            }
            catch (Exception ex)
            {
                //LogTool.WriteErrorLog(ex);
            }

        }

        internal static void WriteErrorLog(string message, Exception ex)
        {
            log.Error(message, ex);
            UploadLog(message+ex.StackTrace, EventType.Error);
        }

        internal static void WriteErrorLog(Exception ex)
        {
            log.Error(ex);
            UploadLog(ex.StackTrace, EventType.Error);
        }

        internal static void WriteErrorLog(string message)
        {
            log.Error(message);
            UploadLog(message, EventType.Error);
        }

        internal static void WriteInfoLog(string message, Exception ex)
        {
            log.Info(message, ex);
        }

        internal static void WriteInfoLog(Exception ex)
        {
            log.Info(ex);
        }

        internal static void WriteInfoLog(string message)
        {
            log.Info(message);
            UploadLog(message, EventType.Information);
        }
    }
}
