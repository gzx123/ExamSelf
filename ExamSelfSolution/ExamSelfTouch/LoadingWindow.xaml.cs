﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ExamSelfTouch.Tools;

namespace ExamSelfTouch
{
    /// <summary>
    /// LoadingWindow.xaml 的交互逻辑
    /// </summary>
    public sealed partial class LoadingWindow : Window
    {
        private static readonly LoadingWindow instance = new LoadingWindow();
        private LoadingWindow()
        {
            InitializeComponent();
            FormTools.SetFormFullScreen(this);
        }

        public static LoadingWindow GetInstance
        {
            get { return instance; }
        }
    }
}
