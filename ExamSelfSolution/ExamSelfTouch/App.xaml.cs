﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using ExamSelfTouch.Tools;
using Xc.Core.Tools;

namespace ExamSelfTouch
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            log4net.Config.XmlConfigurator.Configure();
            AutoStart();
            this.DispatcherUnhandledException += App_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private void AutoStart()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "自助考核系统.exe";
            //bool isex = File.Exists(path);
            RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            //var names = key.GetValueNames();
            key.DeleteValue("自助考核系统", false);
            key.SetValue("自助考核系统", path);
        }





        private async void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            Console.WriteLine("捕获异常" + DateTime.Now.ToShortTimeString());
            Console.WriteLine(e.Exception);
            e.Handled = true;

            LogTool.WriteErrorLog(e.Exception.Message, e.Exception);

        }

        private async void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine("捕获异常" + DateTime.Now.ToShortTimeString());
            Exception exception = e.ExceptionObject as Exception;
            Console.WriteLine(exception.StackTrace);
            LogTool.WriteErrorLog(exception.Message, exception);

        }
    }

}
