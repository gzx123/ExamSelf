﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExamSelfTouch.Tools;
using Xc.Core.Tools;

namespace ExamSelfTouch
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private static MainWindow _instance = new MainWindow();


        private MainWindow()
        {
            InitializeComponent();
            //设置log4net
            LogHelper.SetConfig();
            FormTools.SetFormFullScreen(this);

            this.Closing += MainWindow_Closing;
        }

        static MainWindow() { }

        public static MainWindow GetInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MainWindow();
                    return _instance;
                }
                else return _instance;
            }
        }

        void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

    }
}
