﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamSelfTouch.Model
{
    public class ExamineeInfoApi
    {
        public int status { get; set; }

        public ExamineeInfoRemote examinee { get; set; }

        public List<string> subject { get; set; }
        
    }

    public class ExamineeInfoRemote
    {
        public int autoId { get; set; }
        public string name { get; set; }

        public string WTRoot { get; set; }

        public string WTAdded { get; set; }

        public string photo { get; set; }
    }

    public class ResultData
    {
        public int status { get; set; }
        public string session { get; set; }
    }


    public class ScoreData
    {
        public string session { get; set; }
        public string nr { get; set; }

        public IList<score> items { get; set; }
        
    }

    public class ReturnScore
    {
        public List<score> items { get; set; }

        public int status { get; set; }
    }

    public class score
    {
        public string name { get; set; }
        public decimal value { get; set; }
        
    }


    public enum EnumQuestionType
    {
        Text = 1,
        Image = 2,
        Video = 3
    }

    public class QuestionInfo
    {
        public QuestionInfo()
        {

        }
        public QuestionInfo(int id, string content, string answers, EnumQuestionType enumQuestionType, string otherInfo = null)
        {
            this.Id = id;
            this.Content = content;
            this.Answer = answers;
            this.QuestionType = enumQuestionType;
            this.OtherInfo = otherInfo;
        }
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 问题描述
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 其他信息
        /// </summary>
        public string OtherInfo { get; set; }

        /// <summary>
        /// 答案【单选A/B/C/D】
        /// </summary>
        public string Answer { get; set; }

        /// <summary>
        /// 问题类型
        /// </summary>
        public EnumQuestionType QuestionType { get; set; }

        /// <summary>
        /// 工种
        /// </summary>
        public string WorkType { get; set; }

        /// <summary>
        /// 最后修改时间
        /// </summary>
        public DateTime? ModifyDate { get; set; }
    }

    public class OptionsInfo
    {
        public OptionsInfo()
        {

        }
        public OptionsInfo(int qId, string oTitle, string content, bool isTrue)
        {
            this.QuestionID = qId;
            this.Otitle = oTitle;
            this.Content = content;
            this.IsTrue = isTrue;
        }
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 问题Id
        /// </summary>
        public int QuestionID { get; set; }

        /// <summary>
        /// 选项标识，A/B/C/D
        /// </summary>
        public string Otitle { get; set; }

        public string Content { get; set; }

        /// <summary>
        /// 是否正确选项
        /// </summary>
        public bool IsTrue { get; set; }

    }



    /// <summary>
    /// 服务器日志类
    /// </summary>
    public class ClientTracer
    {
        public int clientId { get; set; }

        public string clientName { get; set; }
        public string content { get; set; }
        public string collection { get; set; }
        public EventType eventType { get; set; }
        public int serial { get; set; }
    }

    public enum EventType
    {
        Information,
        Warning,
        Error
    }
}
