﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamSelfTouch
{
    public static class Global
    {
        /// <summary>
        /// 单位名称
        /// </summary>
        public static string ClientName;
        /// <summary>
        /// log序列号
        /// </summary>
        public static int LogSerial;
    }
}
