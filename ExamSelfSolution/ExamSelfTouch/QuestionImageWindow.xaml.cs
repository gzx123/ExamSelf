﻿using System.Windows;
using ExamSelfTouch.Tools;

namespace ExamSelfTouch
{
    /// <summary>
    /// Description for QuestionImageOptionWindow.
    /// </summary>
    public sealed partial class QuestionImageWindow : Window
    {
        private static QuestionImageWindow _instance = new QuestionImageWindow();
        /// <summary>
        /// Initializes a new instance of the QuestionImageOptionWindow class.
        /// </summary>
        private QuestionImageWindow()
        {
            InitializeComponent();
            FormTools.SetFormFullScreen(this);
            content.FontSize = name.FontSize + 10;
           
        }

        static QuestionImageWindow() { }

        public static QuestionImageWindow GetInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new QuestionImageWindow();
                    return _instance;
                }
                else return _instance;
            }
        }

        public static void ClearInstance()
        {
            _instance.Close();
            _instance = null;
        }
    }
}