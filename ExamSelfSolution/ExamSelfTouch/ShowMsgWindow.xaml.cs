﻿using System.Windows;
using ExamSelfTouch.Tools;

namespace ExamSelfTouch
{
    /// <summary>
    /// Description for ShowMsgWindow.
    /// </summary>
    public partial class ShowMsgWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the ShowMsgWindow class.
        /// </summary>
        public ShowMsgWindow()
        {
            InitializeComponent();
            FormTools.SetFormFullScreen(this);
        }
    }
}