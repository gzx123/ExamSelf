﻿using System.Windows;
using ExamSelfTouch.Tools;

namespace ExamSelfTouch
{
    /// <summary>
    /// Description for QuestionImageWindow.
    /// </summary>
    public sealed partial class QuestionTextWindow : Window
    {
        private static QuestionTextWindow _instance = new QuestionTextWindow();
        /// <summary>
        /// Initializes a new instance of the QuestionImageWindow class.
        /// </summary>
        private QuestionTextWindow()
        {
            InitializeComponent();
            FormTools.SetFormFullScreen(this);
            content.FontSize = name.FontSize+10;
        }

        static QuestionTextWindow() { }

        public static QuestionTextWindow GetInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new QuestionTextWindow();
                    return _instance;
                }
                else return _instance;
            }
        }

        public static void ClearInstance()
        {
            _instance.Close();
            _instance = null;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Console.WriteLine(name.FontSize);
            //System.Console.WriteLine(t.ActualHeight);
        }
    }
}