﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ExamSelfTouch.Message;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace ExamSelfTouch.ViewModel
{
    using ExamSelfTouch.Model;

    public class QuestionTextViewModel : ViewModelBase
    {
        #region 私有字段
        private string _content;
        private int _id;
        private string _answer;
        private ObservableCollection<OptionsInfo> _options;
        #endregion

        #region 属性
        public string Content
        {
            get { return _content; }
            set { _content = value; RaisePropertyChanged("Content"); }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; RaisePropertyChanged("Id"); }
        }

        public string Answer
        {
            get { return _answer; }
            set { _answer = value; RaisePropertyChanged("Answer"); }
        }

        public ObservableCollection<OptionsInfo> Options
        {
            get { return _options; }
            set
            {
                _options = value;
                RaisePropertyChanged("Options");
            }
        }

        private ExamineeInfo _examineeinfo;

        public ExamineeInfo ExamineeInfo
        {
            get { return _examineeinfo; }
            set { _examineeinfo = value; RaisePropertyChanged("ExamineeInfo"); }
        }

        #endregion

        #region 命令

        public RelayCommand NextCommand { get; private set; }
        public RelayCommand<String> SelectCommand { get; private set; }
        #endregion


        public QuestionTextViewModel()
        {
            NextCommand = new RelayCommand(ShowQuestionImage);
            SelectCommand = new RelayCommand<String>(SetSelectShowQuestionImage);

            Messenger.Default.Register<SendTextQuestion>(this, (msg) =>
            {
                this.Content = msg.QuestionInfo.Content;
                this.Answer = msg.QuestionInfo.Answer;
                this.Id = msg.QuestionInfo.Id;
                this.ExamineeInfo = msg.ExamineeInfo;
                Options = new ObservableCollection<OptionsInfo>(msg.Options);
            });
        }

        private void SetSelectShowQuestionImage(string selectAnswer)
        {
            //ShowQuestionImage();
            if (!String.IsNullOrEmpty(selectAnswer))
            {
                Messenger.Default.Send<SendQuestionOverAnswer>(new SendQuestionOverAnswer(Id, EnumQuestionType.Text, selectAnswer));
            }
        }

        /// <summary>
        /// 显示下一页
        /// </summary>
        private void ShowQuestionImage()
        {
            Messenger.Default.Send<SendSingleWindow>(new SendSingleWindow(EnumUseWindowType.Hide, EnumSingleWindow.QuestionTextWindow));
            Messenger.Default.Send<SendSingleWindow>(new SendSingleWindow(EnumUseWindowType.Show, EnumSingleWindow.QuestionImageWindow));
        }
    }
}
