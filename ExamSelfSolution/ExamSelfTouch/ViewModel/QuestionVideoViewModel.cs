﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ExamSelfTouch.Message;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;
using System.Collections;

namespace ExamSelfTouch.ViewModel
{
    using ExamSelfTouch.Model;

    public class QuestionVideoViewModel : ViewModelBase
    {

        #region 私有字段
        private string _content;
        private int _id;
        private string _answer;
        private string _otherInfo;
        private ObservableCollection<OptionsInfo> _options;
        #endregion

        #region 属性
        public string Content
        {
            get { return _content; }
            set { _content = value; RaisePropertyChanged("Content"); }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; RaisePropertyChanged("Id"); }
        }

        public string Answer
        {
            get { return _answer; }
            set { _answer = value; RaisePropertyChanged("Answer"); }
        }

        public ObservableCollection<OptionsInfo> Options
        {
            get { return _options; }
            set
            {
                _options = value;
                RaisePropertyChanged("Options");
            }
        }
        public string OtherInfo
        {
            get { return _otherInfo; }
            set { _otherInfo = value; RaisePropertyChanged("OtherInfo"); }
        }

        private ExamineeInfo _examineeinfo;

        public ExamineeInfo ExamineeInfo
        {
            get { return _examineeinfo; }
            set { _examineeinfo = value; RaisePropertyChanged("ExamineeInfo"); }
        }
        #endregion

        #region 命令

        public RelayCommand PrevCommand { get; private set; }
        public RelayCommand SureCommitCommand { get; set; }
        public RelayCommand<IList> SelectCommand { get; private set; }
        #endregion


        public QuestionVideoViewModel()
        {
            PrevCommand = new RelayCommand(ShowQuestionImage);
            SelectCommand = new RelayCommand<IList>(SetSelectShowQuestionImage);
            SureCommitCommand = new RelayCommand(SureCommitExam);

            Messenger.Default.Register<SendVideoQuestion>(this, (msg) =>
            {
                this.Content = msg.QuestionInfo.Content;
                this.Answer = msg.QuestionInfo.Answer;
                this.Id = msg.QuestionInfo.Id;
                this.OtherInfo = msg.QuestionInfo.OtherInfo;
                this.ExamineeInfo = msg.ExamineeInfo;
                Options = new ObservableCollection<OptionsInfo>(msg.Options);
            });
        }

        private void SetSelectShowQuestionImage(IList obj)
        {
            List<SendQuestionOverAnswer> selectedoptions = new List<SendQuestionOverAnswer>();
            foreach (var item in obj)
            {
                OptionsInfo oinfo = item as OptionsInfo;
                selectedoptions.Add(new SendQuestionOverAnswer(Id, EnumQuestionType.Video, oinfo.Otitle));
            }
            Messenger.Default.Send<List<SendQuestionOverAnswer>>(selectedoptions);
        }

        /// <summary>
        /// 确认提交试卷
        /// </summary>
        private void SureCommitExam()
        {
            if (MessageBox.Show("确认要交卷吗？", "提示", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
            {
                Messenger.Default.Send<SendSingleWindow>(new SendSingleWindow(EnumUseWindowType.Hide, EnumSingleWindow.QuestionVideoWindow));
                //发送消息到主界面，提醒提交试卷，然后主界面结算成绩，发送成绩到
                Messenger.Default.Send("CommitExam");
            }
            
        }

        private void SetSelectShowQuestionImage(string selectAnswer)
        {
            //ShowQuestionImage();
            if (!String.IsNullOrEmpty(selectAnswer))
            {
                Messenger.Default.Send<SendQuestionOverAnswer>(new SendQuestionOverAnswer(Id, EnumQuestionType.Video, selectAnswer));
            }
        }

        /// <summary>
        /// 显示下一页
        /// </summary>
        private void ShowQuestionImage()
        {
            Messenger.Default.Send<SendSingleWindow>(new SendSingleWindow(EnumUseWindowType.Hide, EnumSingleWindow.QuestionVideoWindow));
            Messenger.Default.Send<SendSingleWindow>(new SendSingleWindow(EnumUseWindowType.Show, EnumSingleWindow.QuestionImageWindow));
        }
    }
}
