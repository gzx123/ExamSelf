﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ExamSelfTouch.Message;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System.Collections;

namespace ExamSelfTouch.ViewModel
{
    using ExamSelfTouch.Model;

    public class QuestionImageViewModel : ViewModelBase
    {
        #region 私有字段
        private string _content;
        private int _id;
        private string _answer;
        private string _otherInfo;

        private ObservableCollection<OptionsInfo> _options;
        #endregion

        #region 属性
        public string Content
        {
            get { return _content; }
            set { _content = value; RaisePropertyChanged("Content"); }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; RaisePropertyChanged("Id"); }
        }

        public string Answer
        {
            get { return _answer; }
            set { _answer = value; RaisePropertyChanged("Answer"); }
        }

        public ObservableCollection<OptionsInfo> Options
        {
            get { return _options; }
            set { _options = value; RaisePropertyChanged("Options"); }
        }

        public string OtherInfo
        {
            get { return _otherInfo; }
            set { _otherInfo = value; RaisePropertyChanged("OtherInfo"); }
        }

        private ExamineeInfo _examineeinfo;

        public ExamineeInfo ExamineeInfo
        {
            get { return _examineeinfo; }
            set { _examineeinfo = value; RaisePropertyChanged("ExamineeInfo"); }
        }

        private OptionsInfo _selectedoption;
        public OptionsInfo SelectOption
        {
            get { return _selectedoption; }
            set { _selectedoption = value; RaisePropertyChanged("SelectOption"); }
        }
        #endregion

        #region 命令
        public RelayCommand PrevCommand { get; private set; }
        public RelayCommand NextCommand { get; private set; }
        public RelayCommand<IList> SelectCommand { get; private set; }

        #endregion


        public QuestionImageViewModel()
        {
            PrevCommand = new RelayCommand(ShowQuestionTextWindow);
            NextCommand = new RelayCommand(ShowVideoWindow);
            SelectCommand = new RelayCommand<IList>(SetSelectShowQuestionImage);

            Messenger.Default.Register<SendImageQuestion>(this, (msg) =>
            {
                this.Content = msg.QuestionInfo.Content;
                this.Answer = msg.QuestionInfo.Answer;
                this.Id = msg.QuestionInfo.Id;
                this.OtherInfo = msg.QuestionInfo.OtherInfo;
                this.ExamineeInfo = msg.ExamineeInfo;
                Options = new ObservableCollection<OptionsInfo>(msg.Options);
            });
        }

        private void SetSelectShowQuestionImage(IList obj)
        {
            List<SendQuestionOverAnswer> selectedoptions = new List<SendQuestionOverAnswer>();
            foreach (var item in obj)
            {
                OptionsInfo oinfo = item as OptionsInfo;
                selectedoptions.Add(new SendQuestionOverAnswer(Id, EnumQuestionType.Image, oinfo.Otitle));
            }
            Messenger.Default.Send<List<SendQuestionOverAnswer>>(selectedoptions);

            //Messenger.Default.Send<SendQuestionOverAnswer>(new SendQuestionOverAnswer(Id, EnumQuestionType.Image, selectAnswer));
        }

        private void SetSelectShowQuestionImage()
        {
            Console.WriteLine(SelectOption);
            //ShowVideoWindow();
            //if (!String.IsNullOrEmpty(selectAnswer))
            //{
            //    Messenger.Default.Send<SendQuestionOverAnswer>(new SendQuestionOverAnswer(Id, EnumQuestionType.Image, selectAnswer));
            //}
        }

        private void ShowVideoWindow()
        {
            ShowWindow(EnumSingleWindow.QuestionVideoWindow);
        }

        private void ShowQuestionTextWindow()
        {
            ShowWindow(EnumSingleWindow.QuestionTextWindow);
        }

        private static void ShowWindow(EnumSingleWindow window)
        {
            Messenger.Default.Send<SendSingleWindow>(new SendSingleWindow(EnumUseWindowType.Hide,
                EnumSingleWindow.QuestionImageWindow));
            Messenger.Default.Send<SendSingleWindow>(new SendSingleWindow(EnumUseWindowType.Show,
                window));
        }
    }
}
