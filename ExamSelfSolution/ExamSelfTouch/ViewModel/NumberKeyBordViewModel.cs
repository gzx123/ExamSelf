﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using ExamSelfTouch.Message;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace ExamSelfTouch.ViewModel
{
    public class NumberKeyBordViewModel : ViewModelBase
    {
        #region 命令
        /// <summary>
        /// 点击数字命令
        /// </summary>
        public RelayCommand<string> ClickNumberCommand { get; private set; }
        /// <summary>
        /// 删除数字命令
        /// </summary>
        public RelayCommand DeleteNumberCommand { get; private set; }
        /// <summary>
        /// 确定字母
        /// </summary>
        public RelayCommand<Object> SureNumberCommand { get; private set; }
        #endregion

        #region 参数
        private String _numberText;
        public String NumberText
        {
            get { return _numberText; }
            set { _numberText = value; RaisePropertyChanged("NumberText"); }
        }

        #endregion

        public NumberKeyBordViewModel()
        {
            ClickNumberCommand = new RelayCommand<string>(AppendText);
            DeleteNumberCommand = new RelayCommand(DeleteText);
            SureNumberCommand = new RelayCommand<Object>(SendNumberForMessage);

            Messenger.Default.Register<SendNumberMessage>(this, (msg) =>
            {
                 
                    NumberText = msg.Number;
                 
            });
        }

        /// <summary>
        /// 发送数字到消息
        /// </summary>
        private void SendNumberForMessage(Object obj)
        {
            Messenger.Default.Send<SendNumberMessage>(new SendNumberMessage(NumberText));
            //if (obj is UIElement)
            //{
            //    Messenger.Default.Send<SendNumberMessage>(new SendNumberMessage(NumberText));
            //    //(obj as Window).Close();
            //}
        }

        private void DeleteText()
        {
            if (!String.IsNullOrEmpty(NumberText) && NumberText.Length > 0)
            {
                NumberText = NumberText.Length == 1 ? "" : NumberText.Substring(0, NumberText.Length - 1);
            }
        }

        private void AppendText(string text)
        {
            NumberText += text;
        }

    }
}
