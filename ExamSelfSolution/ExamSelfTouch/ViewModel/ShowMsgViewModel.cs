﻿using ExamSelfTouch.Message;
using ExamSelfTouch.Tools;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;

namespace ExamSelfTouch.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ShowMsgViewModel : ViewModelBase
    {
        private string _msgContent;

        public string MsgContent
        {
            get { return _msgContent; }
            set { _msgContent = value; RaisePropertyChanged("MsgContent"); }
        }

        /// <summary>
        /// Initializes a new instance of the ShowMsgViewModel class.
        /// </summary>
        public ShowMsgViewModel()
        {
            Messenger.Default.Register<SendMsg>(this, (msg) =>
            {
                MsgContent = msg.Msg;
            });
        }
    }
}