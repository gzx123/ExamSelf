﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamSelfTouch.Message
{
    public class SendMsg
    {
        public SendMsg(string showContent)
        {
            Msg = showContent;
        }

        public string Msg { get; set; }
    }
}
