﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamSelfTouch.Message
{
    public class SendNumberMessage
    {
        public SendNumberMessage(string number)
        {
            Number = number;
        }
        public String Number { get; set; }
    }
}
