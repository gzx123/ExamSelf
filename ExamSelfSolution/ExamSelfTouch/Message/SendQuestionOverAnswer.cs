﻿

namespace ExamSelfTouch.Message
{
    using ExamSelfTouch.Model;

    public class SendQuestionOverAnswer
    {
        public SendQuestionOverAnswer(int questionId, EnumQuestionType questionType, string selectAnswer)
        {
            QuestionId = questionId;
            QuestionType = questionType;
            SelectAnswer = selectAnswer;
        }
        public int QuestionId { get; set; }
        public EnumQuestionType QuestionType { get; set; }
        public string SelectAnswer { get; set; }

        public override bool Equals(object obj)
        {
            SendQuestionOverAnswer other = obj as SendQuestionOverAnswer;
            return this.QuestionId == other.QuestionId && this.SelectAnswer == other.SelectAnswer && this.QuestionType == other.QuestionType;
            //return base.Equals(obj);
        }
    }
}
