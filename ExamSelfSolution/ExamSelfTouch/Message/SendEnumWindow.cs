﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamSelfTouch.Message
{
    public class SendSingleWindow
    {
        public SendSingleWindow(EnumUseWindowType useType)
        {
            this.UseType = useType;
            this.SingleWindow = EnumSingleWindow.LoadingWindow;
        }
        public SendSingleWindow(EnumUseWindowType useType, EnumSingleWindow singleWindow)
        {
            this.UseType = useType;
            this.SingleWindow = singleWindow;
        }
        public EnumUseWindowType UseType { get; set; }

        public EnumSingleWindow SingleWindow { get; set; }
    }

    public enum EnumSingleWindow
    {
        MainWindow = 1,
        QuestionTextWindow = 2,
        QuestionImageWindow = 3,
        QuestionVideoWindow = 4,
        LoadingWindow = 5
    }
}
