﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExamSelfTouch.Message
{
    public enum EnumUseWindowType
    {
        /// <summary>
        /// 关闭
        /// </summary>
        Hide = 1,
        /// <summary>
        /// 显示
        /// </summary>
        Show = 2
    }

    public class SendWindow
    {
        public SendWindow(EnumUseWindowType type, Window window)
        {
            this.WindowType = type;
            this.Window = window;
        }
        public EnumUseWindowType WindowType { get; set; }

        public Window Window { get; set; }
    }
}
