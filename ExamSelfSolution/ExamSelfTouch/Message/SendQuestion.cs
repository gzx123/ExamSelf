﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ExamSelfTouch.Message
{
    using ExamSelfTouch.Model;

    public class SendImageQuestion
    {
        public SendImageQuestion(QuestionInfo question,List<OptionsInfo> options,ExamineeInfo examineeInfo)
        {
            this.QuestionInfo = question;
            this.Options = options;
            this.ExamineeInfo = examineeInfo;
        }
        public QuestionInfo QuestionInfo { get; set; }
        public List<OptionsInfo> Options { get; set; }

        public ExamineeInfo ExamineeInfo { get; set; }
    }

    public class SendTextQuestion
    {
        public SendTextQuestion(QuestionInfo question, List<OptionsInfo> options, ExamineeInfo examineeInfo)
        {
            this.QuestionInfo = question;
            this.Options = options;
            this.ExamineeInfo = examineeInfo;
        }
        public QuestionInfo QuestionInfo { get; set; }
        public List<OptionsInfo> Options { get; set; }
        public ExamineeInfo ExamineeInfo { get; set; }
    }
    public class SendVideoQuestion
    {
        public SendVideoQuestion(QuestionInfo question, List<OptionsInfo> options, ExamineeInfo examineeInfo)
        {
            this.QuestionInfo = question;
            this.Options = options;
            this.ExamineeInfo = examineeInfo;
        }
        public QuestionInfo QuestionInfo { get; set; }
        public List<OptionsInfo> Options { get; set; }
        public ExamineeInfo ExamineeInfo { get; set; }
    }

    public class ExamineeInfo
    {
        public string Name { get; set; }
        public ImageSource Header { get; set; }

        public string ExamNumber { get; set; }
        public string ExamType { get; set; }
        
    }
}
