﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xc.Core.Tools;

namespace ExamSelf.Web.Models
{
    /// <summary>
    /// Grid返回数据
    /// </summary>
    public class EasyUIGrid
    {
        public long total { get; set; }

        public object rows { get; set; }
    }

    /// <summary>
    /// Grid请求
    /// </summary>
    public class EasyUIGridRequest
    {
        public EasyUIGridRequest(HttpContextBase context)
        {
            PageSize = context.Request["rows"].ToInt(20);
            PageIndex = context.Request["page"].ToInt(1);
            sortName = context.Request["sort"];
            orderType = context.Request["order"];
        }

        /// <summary>
        /// 每页显示条数
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 排序字段
        /// </summary>
        public string sortName { get; set; }
        /// <summary>
        /// 排序方式
        /// </summary>
        public string orderType { get; set; }
    }


    public class UserRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string OldPassord { get; set; }
        public string NewPassword { get; set; }
        public string CheckCode { get; set; }
        public UserRequest(HttpContextBase context)
        {
            UserName = context.Request["UserName"];
            Password = context.Request["Password"];
            OldPassord = context.Request["OldPassword"];
            NewPassword = context.Request["NewPassword"];
            CheckCode = context.Request["CheckCode"];
        }
        /// <summary>
        /// 验证用户名请求构造
        /// </summary>
        /// <param name="context"></param>
        /// <param name="IsCheckUserName"></param>
        public UserRequest(HttpContextBase context, bool IsCheckUserName)
        {
            if (IsCheckUserName)
            {
                UserName = context.Request["username"];
            }
        }
    }
}