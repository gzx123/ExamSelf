﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamSelf.Web.Models
{
    public class SaveQuestion
    {
        //题目
        public QuestionInfo QuestionInfo { get; set; }

        //选项
        public List<OptionsInfo> OptionsInfoes { get; set; }
    }
}