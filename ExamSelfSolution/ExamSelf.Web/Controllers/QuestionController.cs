﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.SqlClient;
using Xc.Core.Tools;

namespace ExamSelf.Web.Controllers
{
    public class QuestionController : ApiController
    {
        [HttpPost, HttpGet]
        [ActionName("GetQuestion")]
        public async Task<List<QuestionInfo>> GetQuestionMethod(string worktype)
        {
            using (ExamSelfDbContext db = new ExamSelfDbContext())
            {
                try
                {
                    List<QuestionInfo> question = new List<QuestionInfo>();
                    SqlParameter param1 = new SqlParameter("@WorkType", worktype);
                    SqlParameter param2 = new SqlParameter("@WorkType", worktype);
                    SqlParameter param3 = new SqlParameter("@WorkType", worktype);
                    var textQuestion = await db.Database.SqlQuery<QuestionInfo>(
                         " select top 1 *, NewID() as random from QuestionInfoes WHERE QuestionType=1 and WorkType=@WorkType order by random", param1).SingleOrDefaultAsync();
                    var imageQuestion = await db.Database.SqlQuery<QuestionInfo>(
                         " select top 1 *, NewID() as random from QuestionInfoes WHERE QuestionType=2 and WorkType=@WorkType order by random", param2).SingleOrDefaultAsync();
                    var videoQestion = await db.Database.SqlQuery<QuestionInfo>(
                         " select top 1 *, NewID() as random from QuestionInfoes WHERE QuestionType=3 and WorkType=@WorkType order by random", param3).SingleOrDefaultAsync();
                    question.Add(textQuestion);
                    question.Add(imageQuestion);
                    question.Add(videoQestion);
                    return question;
                }
                catch (Exception ex)
                {
                    LogHelper.WriteLog(ex.Message, ex);
                    throw ex;
                }
                
            }
            //List<QuestionInfo> question = new List<QuestionInfo>();
            //question.Add(new QuestionInfo(123, "安全标示识别题：“10KV考核1#线由运行转为检修”操作任务结束后，应当在10KV考核1#线手车131开关操作手柄上挂下方哪种安全标识牌？", "C", EnumQuestionType.Text));
            //question.Add(new QuestionInfo(456, "图中的工作人员违反了哪些安全规定？", "D", EnumQuestionType.Image, "http://7xi4nd.com1.z0.glb.clouddn.com/question.jpg"));
            //question.Add(new QuestionInfo(789, "视频中的工作人员违反了哪项安全规定？", "C", EnumQuestionType.Video, "http://7xi4nd.com1.z0.glb.clouddn.com/question.avi"));
            //return question;
        }
        [HttpPost, HttpGet]
        [ActionName("GetOptions")]
        public async Task<List<OptionsInfo>> GetOptions(List<int> ids)
        {
            using (ExamSelfDbContext db = new ExamSelfDbContext())
            {
                return await db.Options.Where(t => ids.Contains(t.QuestionID)).ToListAsync();
            }
            //List<OptionsInfo> options = new List<OptionsInfo>();
            //options.Add(new OptionsInfo(123, "A", "http://7xi4nd.com1.z0.glb.clouddn.com/A.jpg", false));
            //options.Add(new OptionsInfo(123, "B", "http://7xi4nd.com1.z0.glb.clouddn.com/B.jpg", false));
            //options.Add(new OptionsInfo(123, "C", "http://7xi4nd.com1.z0.glb.clouddn.com/C.jpg", true));
            //options.Add(new OptionsInfo(123, "D", "http://7xi4nd.com1.z0.glb.clouddn.com/D.jpg", false));
            //options.Add(new OptionsInfo(456, "A", "未佩戴安全帽", false));
            //options.Add(new OptionsInfo(456, "B", "未佩戴绝缘手套", false));
            //options.Add(new OptionsInfo(456, "C", "未佩戴绝缘靴", false));
            //options.Add(new OptionsInfo(456, "D", "以上皆是", true));
            //options.Add(new OptionsInfo(789, "A", "未穿工作服", false));
            //options.Add(new OptionsInfo(789, "B", "未佩戴绝缘手套", false));
            //options.Add(new OptionsInfo(789, "C", "未佩戴安全帽", true));
            //options.Add(new OptionsInfo(789, "D", "未佩戴绝缘鞋", false));
            //return options;
        }
    }
}
