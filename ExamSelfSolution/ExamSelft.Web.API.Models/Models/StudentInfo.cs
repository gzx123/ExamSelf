using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Leafing.Data.Definition;

namespace ExamSelft.Web.API.Models.Models
{
    public class StudentInfo : IDbObject
    {
        public int autoId { get; set; }
        public string name { get; set; }

        public string WTRoot { get; set; }

        public string WTAdded { get; set; }

        public string photo { get; set; }
    }

    
}
