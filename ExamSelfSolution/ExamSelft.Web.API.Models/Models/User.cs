﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ExamSelft.Web.API.Models.Models
{
   public class User
    {
        public int Id { get; set; }
       
       [MaxLength(100)]
        public string LoginName { get; set; }
       [MaxLength(100)]
        public string PassWord { get; set; }
    }
}
