﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Leafing.Data.Definition;

namespace DbEntryClassLibrary.Models
{
    public class WebScore : DbObjectModel<WebScore>
    {
        public int autoid { get; set; }
        public string titleId { get; set; }
        public string name { get; set; }
        public decimal value { get; set; }
        /// <summary>
        /// 是否上传完成
        /// </summary>
        public bool IsUploaded { get; set; }
    }
}
